package com.greatlearning.main;

import java.util.Scanner;

import com.greatlearning.admin.AdminDepartment;
import com.greatlearning.hr.HrDepartment;
import com.greatlearning.superdep.SuperDepartment;
import com.greatlearning.tech.TechDepartment;

public class Main {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);		
		System.out.println("Enter the code for the department from the following"+
				"\n" +
					"1.Super Department"+ "\n" +
					"2.Admin Department" + "\n" +
					"3.HR Department" + "\n"+
					"4.Tech Department"
				);
		int code = sc.nextInt();
		sc.close();
		
		switch(code) {
			case 1 :
				SuperDepartment sup = new SuperDepartment();
				String name = sup.departmentName();
				System.out.println("Welcome to " + name +"\n"+ sup.getTodaysWork() + "\n" + sup.getWorkDeadline()+ "\n" + sup.isTodayAHoliday() );
				break;
			case 2 :
				AdminDepartment adm = new AdminDepartment();
				String name2 = adm.departmentName();
				System.out.println("Welcome to " + name2 +"\n"+ adm.getTodaysWork() + "\n" + adm.getWorkDeadline()+ "\n" + adm.isTodayAHoliday() );
				break;
			case 3 :
				HrDepartment hr = new HrDepartment();
				String name3 = hr.departmentName();
				System.out.println("Welcome to " + name3 +"\n"+ hr.getTodaysWork() + "\n" + hr.getWorkDeadline()+ "\n" + hr.doActivity()+ "\n"+ hr.isTodayAHoliday() );
				break;
			case 4 :
				TechDepartment tech = new TechDepartment();
				String name4 = tech.departmentName();
				System.out.println("Welcome to " + name4 +"\n"+ tech.getTodaysWork() + "\n" + tech.getWorkDeadline()+ "\n" + tech.getTechStackInformation() +"\n"+tech.isTodayAHoliday() );
				break;
			default : System.out.println("Invalid input");
				break;
			
		}

	}

}
